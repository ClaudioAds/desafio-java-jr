//import { FormAutorComponent } from './autor/form-autor/form-autor.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { AutorComponent } from './autor/autor.component';
//import { ObraComponent } from './obra/obra.component';

const routes: Routes = [
  { path: '', pathMatch:'full', redirectTo: 'autores' },
  { path: 'autores', loadChildren: './autor/autor.module#AutorModule' },
  { path: 'obras', loadChildren: './obra/obra.module#ObraModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
