import { FormAutorComponent } from './form-autor/form-autor.component';
import { AutorResolverGuard } from './guards/AutorResolverGuard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutorComponent } from './autor.component';

const routes: Routes = [
  { path: '', component: AutorComponent },
  { 
    path: 'novo',
    component: FormAutorComponent,
    resolve: {
      autor: AutorResolverGuard
    }
  },
  {
    path: 'editar/:id',
    component: FormAutorComponent,
    resolve: {
      autor: AutorResolverGuard
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutorRoutingModule {}
