import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Autor } from './autor';
import { CrudService } from '../shared/crud-service';

@Injectable({
  providedIn: 'root'
})
export class AutorService extends CrudService<Autor> {

  constructor(protected http: HttpClient) {
    super(http, `${environment.API}/autores`);
  }

}