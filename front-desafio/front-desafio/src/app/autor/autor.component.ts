import { Component, OnInit, ViewChild } from '@angular/core';
import { AutorService } from './autor-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, empty, Subject, EMPTY } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { Autor } from './autor';
import { AlertModalService } from '../shared/alert-modal.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.css']
})
export class AutorComponent implements OnInit {

  deleteModalRef: BsModalRef;
  @ViewChild('deleteModal', { static: true }) deleteModal;
  error$ = new Subject<boolean>();
  autorSelecionado: Autor;
  autores$: Observable<Autor[]>;

  constructor(
    private service: AutorService,
    private router: Router,
    private alertService: AlertModalService,
    private route: ActivatedRoute,
    private modalService: BsModalService,
  ) {}

  ngOnInit() {
    this.listar();
  }

  listar(){
    return this.autores$ = this.service.list();
  }

  onEdit(id){
    console.log('Editar: '+id)
    this.router.navigate(['editar', id], { relativeTo: this.route});
  }

  atualizaTabela() {
    this.autores$ = this.service.list().pipe(
      catchError(error => {
        console.error(error);
        this.handleError();
        return empty();
      })
    );
  }

  handleError() {
    this.alertService.showAlertDanger('Erro ao carregar autores. Tente novamente.');
  }

  onDelete(autor) {
    this.autorSelecionado = autor;

    const result$ = this.alertService.showConfirm('Confirmacao', `Tem certeza que deseja remover ${autor.nome}?`);
    result$.asObservable()
    .pipe(
      take(1),
      switchMap(result => result ? this.service.remove(autor.id) : EMPTY)
    )
    .subscribe(
      success => {
        this.atualizaTabela();
      },
      error => {
        this.alertService.showAlertDanger('Erro ao remover autor. Tente novamente mais tarde.');
      }
    );
  }

  onConfirmDelete() {
    this.service.remove(this.autorSelecionado.id)
    .subscribe(
      success => {
        this.atualizaTabela();
        this.deleteModalRef.hide();
      },
      error => {
        this.alertService.showAlertDanger('Erro ao remover autor. Tente novamente mais tarde.');
        this.deleteModalRef.hide();
      }
    );
  }

  onDeclineDelete() {
    this.deleteModalRef.hide();
  }

}
