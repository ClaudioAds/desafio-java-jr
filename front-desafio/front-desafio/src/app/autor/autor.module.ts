import { AutorComponent } from './autor.component';
import { FormAutorComponent } from './form-autor/form-autor.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AutorRoutingModule } from './autor-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AutorRoutingModule
  ],
  declarations: [AutorComponent, FormAutorComponent]
})
export class AutorModule { }
