import { Obra } from './../obra/obra';

export class Autor {
    id: number;
    nome: string;
    sexo: string;
    email: string;
    nascimento: string;
    pais: string;
    cpf: string;
    obras: Array<Obra> = [];

    constructor(id: number, nome: string, sexo: string, email: string, nascimento: string, pais: string, cpf: string) {
        this.id = id;
        this.nome = nome;
        this.sexo = sexo;
        this.email = email;
        this.nascimento = nascimento;
        this.pais = pais;
        this.cpf = cpf;
    }

    addObra(obra) {
        this.obras.push(obra);
    }

    removeObra(obra) {
        const index = this.obras.indexOf(obra);
        this.obras.splice(index, obra);
    }
}
