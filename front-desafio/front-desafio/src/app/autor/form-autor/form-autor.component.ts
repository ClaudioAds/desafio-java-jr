import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { map, switchMap } from 'rxjs/operators';
import { AutorService } from '../autor-service.service';

@Component({
  selector: 'app-form-autor',
  templateUrl: './form-autor.component.html',
  styleUrls: ['./form-autor.component.css']
})
export class FormAutorComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private service: AutorService,
    private route: ActivatedRoute
  ) { }    //26661494042, 67741657020, 60193184001

  ngOnInit() {
    //Pegando parametro da rota e preenchendo formulário
    this.route.params.pipe(
      map((params: any) => params['id']),
      switchMap(id => this.service.loadByID(id)),
    )
    .subscribe(autor => this.updateForm(autor));

    //Validação do formulário
    this.form = this.fb.group({
      id: new FormControl(null),
      nome: new FormControl(null, [Validators.required, Validators.minLength(3)]),
      sexo: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      nascimento: new FormControl(null, Validators.required),
      pais: new FormControl(null, Validators.required),
      cpf: new FormControl(null, Validators.required)
    });
  }
  
  hasError(field: string) {
    return this.form.get(field).errors;
  }

  updateForm(autor) {
    this.form.patchValue({
      id: autor.id,
      nome: autor.nome,
      sexo : autor.sexo,
      email : autor.email,
      nascimento : autor.nascimento,
      pais : autor.pais,
      cpf : autor.cpf
    });
  }

  formatData(){
    let data = this.form.value.nascimento.split("-", 3);
    console.log(`Data [0]: ${data[0]}`)
    console.log(`Data [0]: ${data[0].lenght}`)
    console.log(`Data [1]: ${data[1]}`)
    console.log(`Data [2]: ${data[2]}`)
    if(data[0] >= 32)
      this.form.value.nascimento = `${data[2]}/${data[1]}/${data[0]}`
  }

  onSubmit() {
    this.submitted = true;
    //Formatando a data para o padrao DateLocal
    this.formatData()
    console.log(this.form.value);

    let msgSuccess = 'Autor criado com sucesso!';
    let msgError = 'Erro ao criar autor, tente novamente!';
    if (this.form.value.id) {
      msgSuccess = 'Autor atualizado com sucesso!';
      msgError = 'Erro ao atualizar autor, tente novamente!';
    }

    this.service.save(this.form.value).subscribe(
      success => {
        this.location.back();
      },error => console.log(msgError)
    );


  }

  onCancel() {
    this.submitted = false;
    this.form.reset();
    // console.log('onCancel');
  }

  verificaValidTouched(campo){
    return !this.form.get(campo).valid && this.form.get(campo).touched
  }
  aplicaCssErro(campo){
    return{
      'has-error' : this.verificaValidTouched(campo),
      'has-feedback' : this.verificaValidTouched(campo),
    }
  }
}