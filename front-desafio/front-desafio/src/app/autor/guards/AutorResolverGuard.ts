import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Autor } from '../autor';
import { AutorService } from '../autor-service.service';

@Injectable({
  providedIn: 'root'
})
export class AutorResolverGuard implements Resolve<Autor> {
  constructor(private service: AutorService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Autor> {
    if (route.params && route.params['id']) {
      return this.service.loadByID(route.params['id']);
    }

    return of({
        id : null,
        nome : null,
        sexo : null,
        email : null,
        nascimento : null,
        pais : null,
        cpf : null,
        obras: null
    });
    
  }
}
