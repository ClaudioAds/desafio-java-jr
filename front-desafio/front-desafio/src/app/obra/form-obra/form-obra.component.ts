import { Obra } from './../obra';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from "@angular/forms";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { map, switchMap } from 'rxjs/operators';
import { ObraSevice } from '../obra-sevice.service';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/autor/autor';

@Component({
  selector: 'app-form-obra',
  templateUrl: './form-obra.component.html',
  styleUrls: ['./form-obra.component.css']
})
export class FormObraComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private location: Location,
    private service: ObraSevice,
    private route: ActivatedRoute
  ) { }    //26661494042, 67741657020, 60193184001

  ngOnInit() {
    //Pegando parametro da rota e preenchendo formulário
    this.route.params.pipe(
      map((params: any) => params['id']),
      switchMap(id => this.service.loadByID(id)),
    )
    .subscribe(obra => this.updateForm(obra));
    

    //Validação do formulário
    this.form = this.fb.group({
      id: new FormControl(null),
      nome: new FormControl(null, Validators.required),
      descricao: new FormControl(null, [Validators.maxLength(240), Validators.required] ),
      publicacao: new FormControl(null),
      exposicao: new FormControl(null),
    });
  }
  
  hasError(field: string) {
    return this.form.get(field).errors;
  }

  updateForm(obra) {
    this.form.patchValue({
      id: obra.id,
      nome: obra.nome,
      descricao : obra.descricao,
      publicacao : obra.publicacao,
      exposicao : obra.exposicao,
      autores: obra.autores
    });
  }
  
  onSubmit() {

    const obra = new Obra()
    //obra.nome = formValue.nome

    this.submitted = true;
    console.log(this.form.value);

    let msgSuccess = 'Obra criada com sucesso!';
    let msgError = 'Erro ao criar obra, tente novamente!';
    if (this.form.value.id) {
      msgSuccess = 'Obra atualizada com sucesso!';
      msgError = 'Erro ao atualizar obra, tente novamente!';
    }

    this.service.save(this.form.value).subscribe(
      success => {
        this.location.back();
      },error => console.log(msgError)
    );


  }

  onCancel() {
    this.submitted = false;
    this.form.reset();
  }

  verificaValidTouched(campo){
    return !this.form.get(campo).valid && this.form.get(campo).touched
  }
  aplicaCssErro(campo) {
    return{
      'has-error' : this.verificaValidTouched(campo),
      'has-feedback' : this.verificaValidTouched(campo),
    }
  }

  get getAutores(){
    return this.form.get('/autores') as FormArray
  }

  addAutor(){
    const control = <FormArray>this.form.controls['autores']
    control.push(this.fb.group({
      autor: []
    }))
  }

  removeAutor(index:number){
    const control = <FormArray>this.form.controls['autores']
    control.removeAt(index)
  }
}