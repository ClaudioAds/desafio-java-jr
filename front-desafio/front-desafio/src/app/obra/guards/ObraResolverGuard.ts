import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Obra } from '../obra';
import { ObraSevice } from '../obra-sevice.service';

@Injectable({
  providedIn: 'root'
})
export class ObraResolverGuard implements Resolve<Obra> {
  constructor(private service: ObraSevice) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Obra> {
    if (route.params && route.params['id']) {
      return this.service.loadByID(route.params['id']);
    }

    return of({
      id : null,
      nome : null,
      descricao : null,
      publicacao : null,
      exposicao : null,
      autores: null
    });
    }
  }
  