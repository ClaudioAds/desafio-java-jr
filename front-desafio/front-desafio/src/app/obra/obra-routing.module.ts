import { ObraResolverGuard } from './guards/ObraResolverGuard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormObraComponent } from './form-obra/form-obra.component';
import { ObraComponent } from './obra.component';

const routes: Routes = [
  { path: '', component: ObraComponent },
  {
    path: 'novo',
    component: FormObraComponent,
    resolve: {
      curso: ObraResolverGuard
    }
  },
  {
    path: 'editar/:id',
    component: FormObraComponent,
    resolve: {
      curso: ObraResolverGuard
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObraRoutingModule {}
