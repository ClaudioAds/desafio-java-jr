import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Obra } from './obra';
import { environment } from 'src/environments/environment';
import { CrudService } from '../shared/crud-service';


@Injectable({
  providedIn: 'root'
})
export class ObraSevice extends CrudService<Obra> {

  constructor(protected http: HttpClient) {
    super(http, `${environment.API}/obras`);
  }

}