import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, empty, Subject, EMPTY } from 'rxjs';
import { catchError, take, switchMap } from 'rxjs/operators';
import { ObraSevice } from './obra-sevice.service';
import { Obra } from './obra';
import { AlertModalService } from '../shared/alert-modal.service';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-obra',
  templateUrl: './obra.component.html',
  styleUrls: ['./obra.component.css']
})
export class ObraComponent implements OnInit {

  deleteModalRef: BsModalRef;
  obras$: Observable<Obra[]>;
  @ViewChild('deleteModal', { static: true }) deleteModal;
  error$ = new Subject<boolean>();
  obraSelecionada: Obra;

  constructor(
    private service: ObraSevice,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertModalService,
  ) {}

  ngOnInit() {
    this.listar();
  }

  listar(){
    return this.obras$ = this.service.list();
  }

  onEdit(id){
    console.log('Editar: '+id)
    this.router.navigate(['editar', id], { relativeTo: this.route});
  }

  atualizaTabela() {
    this.obras$ = this.service.list().pipe(
      catchError(error => {
        console.error(error);
        this.handleError();
        return empty();
      })
    );
  }

  handleError() {
    this.alertService.showAlertDanger('Erro ao carregar autores. Tente novamente.');
  }

  onDelete(obra) {
    this.obraSelecionada = obra;

    const result$ = this.alertService.showConfirm('Confirmacao', `Tem certeza que deseja remover ${obra.nome}?`);
    result$.asObservable()
    .pipe(
      take(1),
      switchMap(result => result ? this.service.remove(obra.id) : EMPTY)
    )
    .subscribe(
      success => {
        this.atualizaTabela();
      },
      error => {
        this.alertService.showAlertDanger('Erro ao remover obra. Tente novamente mais tarde.');
      }
    );
  }

  onConfirmDelete() {
    this.service.remove(this.obraSelecionada.id)
    .subscribe(
      success => {
        this.atualizaTabela();
        this.deleteModalRef.hide();
      },
      error => {
        this.alertService.showAlertDanger('Erro ao remover autor. Tente novamente mais tarde.');
        this.deleteModalRef.hide();
      }
    );
  }

  onDeclineDelete() {
    this.deleteModalRef.hide();
  }

}
