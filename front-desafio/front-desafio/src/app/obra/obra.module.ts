import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ObraRoutingModule } from './obra-routing.module';
import { FormObraComponent } from './form-obra/form-obra.component';
import { ObraComponent } from './obra.component';

@NgModule({
  imports: [
    CommonModule,
    ObraRoutingModule,
    ReactiveFormsModule,
    ObraRoutingModule
  ],
  declarations: [ObraComponent, FormObraComponent]
})
export class ObraModule { }
