import { Autor } from './../autor/autor';

export class Obra {
    id: number;
    nome: string;
    descricao: string;
    publicacao: string;
    exposicao: string;
    autores: Array<Autor> = []

    constructor() {}

    addAutor(autor) {
        this.autores.push(autor);
    }

    removeAutor(autor){
        const index = this.autores.indexOf(autor);
        this.autores.splice(index, autor);
    }
}