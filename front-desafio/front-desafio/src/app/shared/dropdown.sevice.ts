import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AutorService } from '../autor/autor-service.service';
import { Autor } from '../autor/autor';

@Injectable()
export class DropdownService {
  
    autores$: Observable<Autor[]>;
    lista: Autor[]

    constructor(
        private serviceAutor: AutorService,
    ) { }

    listarAutores(){
        return this.autores$ = this.serviceAutor.list()
    }

    getAutores() {
        return  this.serviceAutor.list()
        .subscribe(
            autor => this.lista = JSON.parse(autor),
            error => 'Erro'
        )
    }

    getCargos() {
        return [
          { nome: 'Dev', nivel: 'Junior', desc: 'Dev Jr' },
          { nome: 'Dev', nivel: 'Pleno', desc: 'Dev Pl' },
          { nome: 'Dev', nivel: 'Senior', desc: 'Dev Sr' }
        ];
      }
}
