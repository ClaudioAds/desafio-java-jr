package br.com.selecaojava.controller;

import br.com.selecaojava.model.Autor;
import br.com.selecaojava.repositories.AutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(value = "*")
public class AutorController {

    @Autowired
    private AutorRepository autorRepository;

    @GetMapping("/autores")
    public List<Autor> getAutores(){
        return autorRepository.findAll();
    }

    @GetMapping("/autores/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) {
        return new ResponseEntity<>(autorRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/autores/{id}")
    public boolean removeAutor(@PathVariable Long id) {
        autorRepository.deleteById(id);
        return true;
    }

    @PutMapping("/autores/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Autor autor, @PathVariable ("id") long id) {

        Optional<Autor> studentOptional = autorRepository.findById(id);

        if (!studentOptional.isPresent())
            return ResponseEntity.notFound().build();

        autor.setId(id);

        autorRepository.save(autor);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/autores")
    public Autor addAutor(@Valid @RequestBody Autor autor) {
        return autorRepository.save(autor);
    }
}
