package br.com.selecaojava.controller;

import br.com.selecaojava.model.Autor;
import br.com.selecaojava.model.Obra;
import br.com.selecaojava.repositories.ObraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(value = "*")
public class ObraController {

    @Autowired
    private ObraRepository obraRepository;

    @GetMapping("/obras")
    public List<Obra> getObras() {
        return obraRepository.findAll();
    }

    @GetMapping("/obras/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) {
        return new ResponseEntity<>(obraRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/obras/{id}")
    public boolean removeObra(@PathVariable Long id) {
        obraRepository.deleteById(id);
        return true;
    }

    @PutMapping("/obras/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Obra obra, @PathVariable("id") long id) {

        Optional<Obra> studentOptional = obraRepository.findById(id);

        if (!studentOptional.isPresent())
            return ResponseEntity.notFound().build();

        obra.setId(id);

        obraRepository.save(obra);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/obras")
    public Obra addObra(@Valid @RequestBody Obra obra) {
        return obraRepository.save(obra);
    }

}
