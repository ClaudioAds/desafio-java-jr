package br.com.selecaojava.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Autor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "autor_seq")
    private Long id;

    @NotNull
    private String nome;

    private String sexo;

    @Email
    @Column(unique = true)
    private String email;

    private String nascimento;

    @NotNull
    private String pais;

    @Column(unique = true)
    private String cpf;

    @ManyToMany
    @JoinTable(name="obras_autor",
            joinColumns = @JoinColumn(name = "autor_id"),
            inverseJoinColumns = @JoinColumn(name = "obra_id"))
    private List<Obra> obras;

    public Autor(@NotNull String nome, String sexo, @Email String email, String nascimento, @NotNull String pais, String cpf, List<Obra> obras) {
        this.nome = nome;
        this.sexo = sexo;
        this.email = email;
        this.nascimento = nascimento;
        this.pais = pais;
        this.cpf = cpf;
        this.obras = obras;
    }

    public Autor(String cláudio, String masculino, String s, String s1, String brasil, String s2) {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<Obra> getObras() {
        return obras;
    }

    public void setObras(List<Obra> obras) {
        this.obras = obras;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Autor obra = (Autor) o;
        return id.equals(obra.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
