package br.com.selecaojava.model;

import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
public class Obra {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "obra-seq")
    private Long id;

    @NotNull
    private String nome;

    @NotNull
	@Size(max = 240)
    private String descricao;

    private String publicacao;

    private String exposicao;

    @ManyToMany(mappedBy = "obras")
    private List<Autor> autores;

    public Obra(@NotNull String nome, @NotNull @Size(max = 240) String descricao, String publicacao, String exposicao, List<Autor> autores) {
        this.nome = nome;
        this.descricao = descricao;
        this.publicacao = publicacao;
        this.exposicao = exposicao;
        this.autores = autores;
    }

    public Obra(String monalisa, String pintura, String s, Object o, Object o1) {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(String publicacao) {
        this.publicacao = publicacao;
    }

    public String getExposicao() {
        return exposicao;
    }

    public void setExposicao(String exposicao) {
        this.exposicao = exposicao;
    }

    public List<Autor> getAutores() {
        return autores;
    }

    public void setAutores(List<Autor> autores) {
        this.autores = autores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Obra obra = (Obra) o;
        return id.equals(obra.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
