package br.com.selecaojava.repositories;

import br.com.selecaojava.model.Autor;
import br.com.selecaojava.model.Obra;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutorRepository extends JpaRepository<Autor, Long>{

}
