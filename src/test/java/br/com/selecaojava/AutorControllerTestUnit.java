package br.com.selecaojava;

import br.com.selecaojava.model.Autor;
import br.com.selecaojava.repositories.AutorRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AutorControllerTestUnit {
    @Autowired
    private AutorRepository autorRepository;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void creatUser(){
        Autor autor = new Autor("Cláudio", "Masculino", "claudio@wer", "10/08/1897", "Brasil", "058.469.820-85", null);
        this.autorRepository.save(autor);
        assertThat(autor.getNome()).isEqualTo("Cláudio");
        assertThat(autor.getSexo()).isEqualTo("Masculino");
        assertThat(autor.getEmail()).isEqualTo("claudio@wer");
        assertThat(autor.getNascimento()).isEqualTo("10/08/1897");
        assertThat(autor.getPais()).isEqualTo("Brasil");
        assertThat(autor.getCpf()).isEqualTo("058.469.820-85");

    }

    @Test
    public void removeAutor(){
        Autor autor = new Autor("Cláudio", "Masculino", "claudio@wer", "10/08/1897", "Brasil", "058.469.820-85", null);
        this.autorRepository.save(autor);
        autorRepository.delete(autor);
        assertThat(autorRepository.findById(autor.getId())).isNotNull();
    }

}
