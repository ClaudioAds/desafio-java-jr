package br.com.selecaojava;

import br.com.selecaojava.model.Autor;
import br.com.selecaojava.repositories.AutorRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static java.util.Arrays.asList;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AutorEndPointTestIntegration {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private AutorRepository autorRepository;

    @Autowired
    private MockMvc mockMvc;

    @TestConfiguration
    static class config{
        public RestTemplateBuilder restTemplateBuilder(){
            return new RestTemplateBuilder();
        }
    }

    @Test
    public void listUsersWhenUserForId(){
        ResponseEntity<String> response = testRestTemplate.getForEntity("/api/autores/1", String.class);
        Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    public void listUsersCorrects(){
        List<Autor> autores = asList(new Autor("Cláudio", "Masculino", "claudio@wer", "10/08/1897", "Brasil", "058.469.820-85", null),
                new Autor("Marcela", "Feminina", "claudio@43214gsgf", "10/08/1897", "Espanha", "242.910.500-46", null));

        BDDMockito.when(autorRepository.findAll()).thenReturn(autores);
        ResponseEntity<String> response = testRestTemplate.getForEntity("/api/autores", String.class);
        Assertions.assertThat(response.getStatusCodeValue()).isEqualTo(200);
    }
}
