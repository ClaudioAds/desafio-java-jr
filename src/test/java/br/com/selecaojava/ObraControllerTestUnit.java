package br.com.selecaojava;

import br.com.selecaojava.model.Autor;
import br.com.selecaojava.model.Obra;
import br.com.selecaojava.repositories.AutorRepository;
import br.com.selecaojava.repositories.ObraRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ObraControllerTestUnit {
    @Autowired
    private ObraRepository obraRepository;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void addObra(){
        Obra obra = new Obra("Monalisa", "Pintura", "10/11/1578", null, null);
        this.obraRepository.save(obra);
        assertThat(obra.getNome()).isEqualTo("Monalisa");
        assertThat(obra.getDescricao()).isEqualTo("Pintura");
        assertThat(obra.getPublicacao()).isEqualTo("10/11/1578");
        assertThat(obra.getExposicao()).isEqualTo(null);
        assertThat(obra.getAutores()).isEqualTo(null);

    }

    @Test
    public void removeObra(){
        Obra obra = new Obra("Monalisa", "Pintura", "10/11/1578", null, null);
        this.obraRepository.save(obra);
        obraRepository.delete(obra);
        assertThat(obraRepository.findById(obra.getId())).isNotNull();
    }

}
